# beacon_triangulation

Code for determining position for (i)Beacons.

## Sub projects
###beacon
mock beacon code. Can simulate noisy distance and rssi data given a distance.
###room
mock signals for multiple beacons in a room or space.

###Triangulator
Java program that calculates the estimated position. 
Requirements:

 - Java 8
 
Run with `gradle run`

POST the following to `http://localhost:4567/getPositionEstimate`

```json
{
  "x": 5,
  "y": 6
}
```
And you will get a response:

```json
{
  "position": {
    "x": 0.3999999999999999,
    "y": 0.5480769161256961
  },
  "beacons": []
}
```

Where x and y are the estimations of your position. The beacons array is a list of all beacons with their respective estimated distance to you. It is empty because of a bug.