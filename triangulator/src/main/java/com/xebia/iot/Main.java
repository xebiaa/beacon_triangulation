package com.xebia.iot;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jayway.restassured.path.json.JsonPath;
import com.vividsolutions.jts.geom.Point;

import static spark.Spark.post;


public class Main {
    private final static Logger LOG = LoggerFactory.getLogger(Main.class);
    static Triangulator triangulator;

    public static void main(String... args) {
        post("/getPositionEstimate", (req, res) -> {
            String body = req.body();
            double x = JsonPath.from(body).getDouble("x") / 100;
            double y = JsonPath.from(body).getDouble("y") / 100;
            triangulator = new Triangulator(x, y);
            Point intersection = triangulator.getIntersection();
            JsonObjectBuilder builder = Json.createObjectBuilder().add("position",
                    Json.createObjectBuilder()
                            .add("x", intersection.getX())
                            .add("y", intersection.getY()).build());
            adder(builder);

            return builder.build().toString();
        });

    }

    public static void adder(JsonObjectBuilder builder) {

        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        arrayBuilder
                .add(triangulator.getDistance("a"))
                .add(triangulator.getDistance("b"))
                .add(triangulator.getDistance("c"))
                .add(triangulator.getDistance("d")).build();
        builder.add("beacons", arrayBuilder);

    }
}
