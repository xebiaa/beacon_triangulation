package com.xebia.iot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ResponseBody;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.util.GeometricShapeFactory;

import static com.jayway.restassured.RestAssured.given;

public class Triangulator {
    private final static Logger LOG = LoggerFactory.getLogger(Triangulator.class);
    private GeometryFactory geometryFactory = new GeometryFactory();

    public double x;
    public double y;

    Map<String, Coordinate> beacons = new HashMap<>();

    public Coordinate beaconA = new Coordinate(0.4, 0.4);
    public Coordinate beaconB = new Coordinate(0.4, 4.3);
    public Coordinate beaconC = new Coordinate(8.25, 4.3);
    public Coordinate beaconD = new Coordinate(8.25, 4.0);

    public Triangulator(double x, double y) {
        this.x = x;
        this.y = y;

        beacons.put("a", beaconA);
        beacons.put("b", beaconB);
        beacons.put("c", beaconC);
        beacons.put("d", beaconD);
    }


    public Double getDistance(String beacon) {
        int xi = (int) x;
        int yi = (int) y;
        String url = String.format("http://localhost:8080/getRssi?uuid=%s&x=%s&y=%s", beacon, xi, yi);
        LOG.debug(url);
        ResponseBody body = given().contentType(ContentType.JSON).post(url).body().prettyPeek();
        Double result = body.jsonPath().getDouble("distance");

        return result;
    }

    public Polygon getBeaconRadius(Coordinate beacon, double distance) {
        GeometricShapeFactory geometricShapeFactory = new GeometricShapeFactory();
        geometricShapeFactory.setCentre(beacon);
        geometricShapeFactory.setHeight((distance + 2) * 2);
        geometricShapeFactory.setWidth((distance + 2) * 2);
        Polygon circle = geometricShapeFactory.createCircle();
        return circle;
    }

    public Point getIntersection() {
        List<Geometry> geometries = new ArrayList<>();
        for (Map.Entry<String, Coordinate> beacon : beacons.entrySet()) {
            Polygon a = getBeaconRadius(beacon.getValue(), getDistance(beacon.getKey()));
            geometries.add(a);

        }
        Geometry geometry = geometries.get(0);
        for (int i = 1; i < geometries.size(); i++) {
            geometry = geometry.intersection(geometries.get(i));
        }

        return geometry.getCentroid();
    }

    public Polygon polygon() {
        return null;
    }
}
