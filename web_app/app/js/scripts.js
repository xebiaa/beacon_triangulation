var margin = {
  left: 5,
  top: 5
};
var phoneSize = {
  width: 75,
  height: 75
};
var beaconSize = {
  width: 50,
  height: 50
};

var initPosition = { x: 250, y: 200 };

var container = d3.select('#container')
  .append('svg')
  .attr('width', 900)
  .attr('height', 500)
  .append('g')
  .attr('transform', 'translate(' + margin.left + ', ' + margin.top + ')');

var room = container.append('rect')
  .attr('width', 865)
  .attr('height', 470)
  .attr('cursor', 'pointer')
  .attr('class', 'room')
  .on('click', setCurrentPosition);


// The estimated position of the user
var user = container.append('g')
  .datum(initPosition)
  .attr('transform', translate);
user
    .append('image')
    .attr("xlink:href", "img/phone.svg")
    .attr('x', -phoneSize.width / 2)
    .attr('y', -phoneSize.height / 2)
    .attr('width', phoneSize.width)
    .attr('height', phoneSize.height);

// The real position of the user
var positionIcon = container.append('circle')
  .datum(initPosition)
  .attr('cx', x)
  .attr('cy', y)
  .attr('r', 10)
  .attr('class', 'position');

// The iBeacons
var beacons = container.selectAll('.beacon')
  .data([
    { x: 40, y: 40 },
    { x: 40, y: 430 },
    { x: 825, y: 430 },
    { x: 825, y: 40 }
  ])
  .enter().append('g')
  .attr('class', 'beacon')
  .attr('transform', translate)
    .append('image')
    .attr("xlink:href", "img/ibeacon.svg")
    .attr('x', -beaconSize.width / 2)
    .attr('y', -beaconSize.height / 2)
    .attr('width', beaconSize.width)
    .attr('height', beaconSize.height);

d3.select('#getNewPosition').on('click', getNewPosition);

setCurrentPositionInTable();

function setCurrentPositionInTable() {
  updateTable([['Current position', positionIcon.datum()]]);
}

function setCurrentPosition() {
  positionIcon
    .datum({
      x: d3.event.offsetX,
      y: d3.event.offsetY
    })
    .attr('cx', x)
    .attr('cy', y);

  setCurrentPositionInTable();
}

function getNewPosition() {
  var setData = {
    position: positionIcon.datum(),
    beacons: beacons.data()
  };

  d3.json('http://localhost:4567/getPositionEstimate')
    .header("Content-Type", "application/json")
    .post(JSON.stringify(positionIcon.datum()), updateEstPosition);
  //d3.json('mock/getPosition.json', updateEstPosition);
}

function mapToTable(result) {
  var tableData = [
    ['Current position', positionIcon.datum()],
    ['Estimated position', result.position]
  ];
  _.each(result.beacons, function (d, i) {
    tableData.push(['Distance to iBeacon ' + (i+1), d]);
  });
  return tableData;
}

function updateTable(data) {
  var tbody = d3.select('table.info tbody');
  var rows = tbody.selectAll('tr')
    .data(data)
    .enter().append('tr');
  //rows.exit().remove();

  var cells = rows.selectAll('td')
    .data(function (d) {
      return d;
    })
    .enter().append('td')
    .text(value);


}

function updateEstPosition(error, result) {
  user
    .datum(result.position)
    .attr('transform', function (d) {
      return 'translate(' + d.x * 100 + ', ' + d.y * 100 + ')';
    });

  updateTable(mapToTable(result));
}

function x(d) {
  return d.x;
}

function y(d) {
  return d.y;
}

function value(d) {
  return _.isObject(d) ? '(' + d.x + ', ' + d.y + ')' : d;
}

function translate(d) {
  return 'translate(' + d.x + ', ' + d.y + ')';
}
