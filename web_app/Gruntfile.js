module.exports = function (grunt) {

  require('load-grunt-tasks')(grunt);
  var paths = {
    app: require('./bower.json').appPath || 'app',
    test: 'test',
    dist: 'dist'
  };

  grunt.initConfig({
    paths: paths,
    watch: {
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= paths.app %>/{,*/}*.html',
          '<%= paths.app %>/css/{,*/}*.css',
          '<%= paths.app %>/js/{,*/}*.js'
        ]
      }
    },

    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('app'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect().use(
                '/app/css',
                connect.static('./app/css')
              ),
              connect.static(paths.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 9001,
          middleware: function (connect) {
            return [
              connect.static('app'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(paths.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= paths.dist %>'
        }
      }
    },


  });

  grunt.registerTask('serve', 'Compile then start a connect web server', function () {
    grunt.task.run([
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('default', [
    'serve'
  ]);

};