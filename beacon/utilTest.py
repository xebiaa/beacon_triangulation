import unittest
import util


class UtilTestCase(unittest.TestCase):
    def test_clamping(self):
        self.assertEqual(util.clamp(1.0, 1.5, 2.2), 1.5)
        self.assertEqual(util.clamp(1.0, 0.4, 2.2), 1.0)
        self.assertEqual(util.clamp(1.0, 4.4, 2.2), 2.2)

    def test_random_guassian(self):
        for i in range(0, 100):
            val = util.getRangedGaussian()
            self.assertTrue(val >= -1.0)
            self.assertTrue(val <= 1.0)


if __name__ == '__main__':
    unittest.main()
