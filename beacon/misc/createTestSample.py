#!/bin/python
"""
Code used to generate "truth" values for spreadsheet
"""

import rssiCalculation

def printTable():
    """
    Print expected rssi table for fixed set of distances
    :return:
    """
    txPower = -60.0 # at 1m == refDistance
    for dCm in range(50, 501, 50):
        d = dCm / 100.0 # cm -> m
        rssi = rssiCalculation.distanceToRssi(txPower, d)
        print "%.2f\t\t %d" % (d, rssi)

printTable()