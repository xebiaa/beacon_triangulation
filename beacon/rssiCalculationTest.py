import unittest
import rssiCalculation


class RssiCalculationTestCase(unittest.TestCase):
    def test_rssi_calculation_for_calibration_distance(self):
        txPower = 60
        calibrateDistance = 1
        self.assertEqual(rssiCalculation.distanceToRssi(txPower, calibrateDistance), txPower)

    def test_round_trip(self):
        txPower = 60
        distance = 77
        rssi = rssiCalculation.distanceToRssi(txPower, distance)
        distanceTrip = rssiCalculation.rssiToDistance(txPower, rssi)
        self.assertAlmostEqual(distance, distanceTrip)


if __name__ == '__main__':
    unittest.main()
