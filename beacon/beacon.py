"""
Actual beacon simulation code
"""

import util
import rssiCalculation

# Expected deviations, src: https://community.estimote.com/hc/en-us/articles/201029223-Beacons-signal-characteristics-
# 20cm
#   deviation 5-6 cm
# 1m
#   deviation 15 cm
# >10 m
#   deviation 2-3 m
#
# --> Deviation is about 20% of actual distance
def getDistanceBeacon(distance, noise=True):
    """
    calculate noisy distance for a given actual distance
    :param distance: as the crow flies in m
    :return: distance in m with noise
    """
    rand = util.getRangedGaussian() # between -1 and 1
    noise = rand * distance * 0.2 # +- 20% of actual distance
    return distance + noise


txPower = -60 # at 1m == refDistance

def getRssiBeacon(distance, noise=True):
    """
    calculate rssi with noise
    :param distance: as the crow flies in m
    :return: rssi in dB
    """
    noiseDistance = getDistanceBeacon(distance)
    return rssiCalculation.distanceToRssi(txPower, noiseDistance)
