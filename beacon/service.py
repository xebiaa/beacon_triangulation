#!/bin/python
import web
import json

import beacon

urls = (
    '/', 'index',
    '/distance/(\d+)', 'get_distance',
    '/rssi/(\d+)', 'get_rssi'
)

class index:
    """
    Show list of routes
    """
    def GET(self):
        return "\n".join(urls)

class get_distance:
    # Example reading parameter test from POST request
    # def POST(self, distance):
    #     data = web.input()
    #     return data.test

    def GET(self, dCm):
        d = beacon.getDistanceBeacon(float(dCm) / 100.0)
        web.header('Content-Type', 'application/json')
        return json.dumps({
            'distance': d,
            'noise': True
        })

class get_rssi:
    def GET(self, d):
        rssi = beacon.getRssiBeacon(float(d) / 100.0)
        return json.dumps({
            'rssi': rssi,
            'noise': True
        })

if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()