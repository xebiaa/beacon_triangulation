import random

def clamp(minimum, x, maximum):
    return max(minimum, min(x, maximum))

def getRangedGaussian():
    """
    Get random number with gaussian distribution clamped at -1 and 1.
    :return: float
    """
    mu = 0
    sigma = 0.33
    rand = random.gauss(mu, sigma)
    return clamp(-1, rand, 1)