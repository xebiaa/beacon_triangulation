import math

refDistance = 1.0   # 1m    == d0 in formula
pathLoss = 3.0        #       == n in formula

def rssiToDistance(txPower, rssi):
    c1 = txPower
    c2 = pathLoss
    return refDistance * math.pow(10, (c1-rssi) / (10 * c2))

def distanceToRssi(txPower, distance):
    c1 = txPower
    c2 = pathLoss
    distanceDiff = distance / refDistance
    return c1 - 10 * c2 * math.log10(distanceDiff)
