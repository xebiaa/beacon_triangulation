import math

def euclidian_distance(pointA, pointB):
    dx = pointA.x - pointB.x
    dy = pointA.y - pointB.y
    return math.sqrt(dx * dx + dy * dy)