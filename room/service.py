#!/bin/python
import web
import json

from room import Room
from dataContainer import *

urls = (
    '/', 'index',
    '/addBeacon/(\w+)/(\d+)/(\d+)', 'addBeacon',
    '/getRssi', 'getRssi',
    '/list', 'list',
     '/reset', 'reset'
)

room = Room()


class index:
    """
    Show list of routes
    """
    def GET(self):
        return "\n".join(urls)

class getRssi:

    def POST(self):
        uuid = web.input().uuid
        point = Point(float(web.input().x), float(web.input().y))

        distance = room.getDistance(uuid, point)
        rssi = room.getRssi(uuid, point)

        return json.dumps({
            'distance': distance,
            'rssi': rssi
        })

class addBeacon:

    def GET(self, label, xCm, yCm):
        beacon = Beacon(label, Point(float(xCm) / 100.0, float(yCm) / 100.0))
        nr = room.addBeacon(beacon)
        return "Room now has %d beacon(s)" % nr

class list:
    def GET(self):
        beacons = room.getBeacons()
        web.header('Content-Type', 'application/json')
        return json.dumps(beacons, indent=4, separators=(',', ': '))

class reset:
    def GET(self):
        beacons = room.reset()
        return "done"

# room.addBeacon(Beacon("a", Point(0, 0)) )
# room.addBeacon(Beacon("b", Point(10, 0)) )
# room.addBeacon(Beacon("d", Point(0, 10)) )
# room.addBeacon(Beacon("c", Point(10, 10)) )

# same as frontend
room.addBeacon(Beacon("a", Point(0.4, 0.4)))
room.addBeacon(Beacon("b", Point(0.4, 4.3)))
room.addBeacon(Beacon("d", Point(8.25, 4.3)))
room.addBeacon(Beacon("c", Point(8.25, 4.0)))


# print room.getDistance("a", Point(16, 2))
# import calculationAdapter
# print calculationAdapter.getRssiDistance(5)

if __name__ == '__main__':
    app = web.application(urls, globals())
    app.run()