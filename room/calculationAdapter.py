import requests

from settings import CALCULATION_SERVICE

# example with data
# data = '{"query":{"bool":{"must":[{"text":{"record.document":"SOME_JOURNAL"}},{"text":{"record.articleTitle":"farmers"}}],"must_not":[],"should":[]}},"from":0,"size":50,"sort":[],"facets":{}}'
# response = requests.get(url, data=data)

distancePath = 'distance/%d'
rssiPath = 'rssi/%d'

def getUrl(path):
    host = CALCULATION_SERVICE.get('host')
    port = CALCULATION_SERVICE.get('port')
    return 'http://' + host + ':' + str(port) + '/' + path

def getNoisyDistance(distance):
    url = getUrl(distancePath)
    url = url % int(distance * 100)
    response = requests.get(url)
    distance = response.json().get('distance')
    return distance

def getNoisyRssi(distance):
    url = getUrl(rssiPath)
    url = url % int(distance * 100)
    response = requests.get(url)
    rssi = response.json().get('rssi')
    return rssi