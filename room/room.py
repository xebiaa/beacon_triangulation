import util

import calculationAdapter

class Room:
    """
    Room with beacons positioned around
    """
    beacons = []

    def findBeacon(self, uuid):
        for beacon in self.beacons:
            if beacon.uuid == uuid:
                return beacon
        return None


    def addBeacon(self, beacon):
        """
        Add if not existing yet
        :param uuid:
        :param point: Point
        :return: number of beacons in room
        """
        if not self.findBeacon(beacon.uuid):
            self.beacons.append(beacon)
        return len(self.beacons)

    def getBeacons(self):
        return self.beacons

    def reset(self):
        beacons = []


    def beaconDistance(self, uuid, point):
        beacon = self.findBeacon(uuid)
        if not beacon:
            raise Exception("Beacon not found!")
        return util.euclidian_distance(beacon.point, point)

    def getDistance(self, uuid, point):
        """
        See get getRssi documentation but distance
        """
        d = self.beaconDistance(uuid, point)
        return calculationAdapter.getNoisyDistance(d)

    def getRssi(self, uuid, point):
        """
        Calculate rssi for point with respect to a known beacon.
        :param uuid:
        :param point: Point, point of interest to calculate rssi reading
        :return:
        """
        d = self.beaconDistance(uuid, point)
        return calculationAdapter.getNoisyRssi(d)
