import unittest
from dataContainer import Point

import util

class UtilTest(unittest.TestCase):
    def test_euclidian(self):
        a = Point(2, 3)
        b = Point(-2, 4)
        self.assertAlmostEqual(4.12311, util.euclidian_distance(a, b), 5)

if __name__ == '__main__':
    unittest.main()
