from collections import namedtuple

Point = namedtuple('Point', ['x', 'y'])
Beacon = namedtuple('Beacon', ['uuid', 'point'])